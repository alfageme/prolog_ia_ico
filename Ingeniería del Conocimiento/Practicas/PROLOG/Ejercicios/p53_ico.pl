% Práctica 5 - Ingeniería del Conocimiento 
% Samuel Alfageme Sainz
% -----------------------------------------
% METAINTERPRETES : Ejercicio 3
%
% Modificacion del metainterprete vanilla para que muestre la traza de las metas
% 	que va resolviendo, mostrando el nivel de las mismas. Ejemplo:

/* METAINTERPRETE + TRACE : 
 * 1 ?- solve_traza(valor(w3,X)).
 *   1 conectado(w3,w2)
 *   1 valor(w2,_G374)
 *     2 conectado(w2,w1)
 *     2 valor(w1,1)
 * X = 1 ;
 *     2 valor(w1,_G374)
 * false.
 */  

% Para alcanzar esta solución se ha consultado el Cap.23 de Ivan Bratko:
% Prolog Programming for Artificial Intelligence 3ªEd.
solve(true,Prof):-!.
solve((A,B),Prof):-!,solve(A,Prof),solve(B,Prof).
solve(A,Prof):-
	clause(A,B),
	NuevaProf is Prof + 1,
	print_traza(A,Prof),
	solve(B,NuevaProf).

print_traza(Meta,Prof):-
	tab(Prof),write(Prof),
	put_char(" "),write(Meta),nl.

solve_traza(Valor):-solve(Valor,0).

valor(w1,1).
conectado(w2,w1).
conectado(w3,w2).
valor(W,X):-conectado(W,V),valor(V,X).
