% Práctica 4 - Ingeniería del Conocimiento 
% Samuel Alfageme Sainz
% -----------------------------------------
% JUEGO DE LAS 3 EN RAYA - Movimiento de defensa

% Base de Conocimiento: 
% Numeración en el tablero: 
%	1 | 2 | 3
%	- + - + -
%	4 | 5 | 6
%	- + - + -
%	7 | 8 | 9
% Representación del tablero = [1,2,3,4,5,6,7,8,9]
% Representación de las casillas: vacias: "" , caras: o , cruces: x
% Alineaciones Horizontales
horizontal([1,2,3]).
horizontal([4,5,6]).
horizontal([7,8,9]).
% Alineaciones Verticales
vertical([1,4,7]).
vertical([2,5,8]).
vertical([3,6,9]).
% Alineaciones Diagonales
diagonal([1,5,9]).
diagonal([3,5,7]).
% Cualquiera de las alineaciones anteriores es una jugada ganadora
jugada_ganadora(X):-horizontal(X).
jugada_ganadora(X):-vertical(X).
jugada_ganadora(X):-diagonal(X).

% Puesta de una pieza X en una posicion concreta N del tablero T
% Uso del predicado arg/3 cuyo primer argumento: n={1..Aridad} provoca que el 
% tercer argumento unifique con el n-esimo argumento del predicado pasado como 
% segundo argumento. En el caso de una lista: la cabeza (1) o la cola (2).
poner_pieza(1,T,X):-arg(1,T,X).
poner_pieza(N,T,X):- N > 1, arg(2,T,Y),M is N-1, poner_pieza(M,Y,X).

% Estados posibles de las casillas: vacias, caras o cruces
vacio(C,T):-poner_pieza(C,T,"").
cruz(C,T) :-poner_pieza(C,T,x).
cara(C,T) :-poner_pieza(C,T,o).

% Para evitar una amenaza, colocamos la cara en la posicion vacia de una posible
% linea de las cruces:
evita_amenaza([X,Y,Z],M,X):-vacio(X,M),cruz(Y,M),cruz(Z,M).
evita_amenaza([X,Y,Z],M,Y):-cruz(X,M),vacio(Y,M),cruz(Z,M).
evita_amenaza([X,Y,Z],M,Z):-cruz(X,M),cruz(Y,M),vacio(Z,M).

% Si no existe ningún movimiento forzoso el predicado devuelve false, en caso de
% que haya más de uno devuelve todos.
movimiento_forzoso_caras(Tablero,Casilla):-
		jugada_ganadora(A),evita_amenaza(A,Tablero,Casilla).

% Ejemplo de movimientos forzosos: 

%	x | " | x
%	- + - + -
%	o | " | "
%	- + - + -
%	x | " | o

% Consulta:
% ?- movimiento_forzoso_caras([x,"",x,o,"","",x,"",o],P).
% Respuestas:
% P = 2 ;
% P = 5 ;
% false.
