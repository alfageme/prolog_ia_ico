/* 	Meta int�rprete Vanilla	 			*/
/*							*/
/*	2011.						*/
/*	Dpto. de Inform�tica. 				*/
/*	Universidad de Valladolid.			*/
/*							*/
/*	tomado de:					*/
/*	L. Sterling, E. Shapiro				*/
/*	The Art of Prolog, Second Edition		*/
/*	The MIT Press 1994				*/
/*	ISBN 0-262-19338-8				*/





/*	Meta int�rprete Vanilla				*/
/*							*/


solve(true):-!.

/*	Para obtenr la regla de c�mputo 1er literal	*/
/*	a la derecha, basta resolver primero		*/
/*	la parte derecha de una emta conjuntiva		*/
solve((A,B)) :-!, solve(B), solve(A).
solve(A) :- clause(A, B), solve(B).


/* Base de Conocimiento: conexi�n unidireccional  */

valor(w1, 1).
conectado(w2, w1).
conectado(w3, w2).
valor(W,X):-conectado(W,V), valor(V,X).


/*   QU� OCURRE SI PLANTEAIS LA META:			*/
/*	solve(valor(X,Y)) y despu�s de obtener la 	*/
/*	tercera	respuesta quer�is buscar la cuarta?	*/


/*	POR QU�?					*/
