/* 	Meta int�rprete Vanilla	 			*/
/*							*/
/*	2009.						*/
/*	Dpto. de Inform�tica. 				*/
/*	Universidad de Valladolid.			*/
/*							*/
/*	tomado de:					*/
/*	L. Sterling, E. Shapiro				*/
/*	The Art of Prolog, Second Edition		*/
/*	The MIT Press 1994				*/
/*	ISBN 0-262-19338-8				*/





/*	Meta int�rprete Vanilla	predefinidos traza	*/
/*							*/


builtin(A is B).	builtin(A > B).		builtin(A < B).
builtin(A = B).		builtin(A =:= B).	builtin(A =< B).
builtin(A >= B).	builtin(functor(T, F, N)).
builtin(read(X)).	builtin(write(X)).

/*  	La meta inicial tiene profundidad cero		*/
solve_traza(Meta)  :- solve_traza(Meta, 0).
solve_traza(true, Prf) :- !.
solve_traza((A,B), Prf) :- 
	!, solve_traza(A, Prf), solve_traza(B, Prf).
solve_traza(A, Prf):- 
	builtin(A), !, A, display(A, Prf), nl.
/*	Esta es la �nica cl�usula del meta int�rprete	*/
/*	que genera una meta de mayor profundidad	*/
solve_traza(A, Prf) :- 
	clause(A,B), display(A, Prf), nl, 
	Prf1 is Prf +1, solve_traza(B, Prf1).

/*	Para identar la meta				*/	
display(A, Prf):-
	Espacios is 3*Prf, tab(Espacios), write(Prf), tab(1), write(A).




/* Base de Conocimiento: conexi�n unidireccional  */

valor(w1, 1).
conectado(w2, w1).
conectado(w3, w2).
valor(W,X):-conectado(W,V), valor(V,X).
