﻿/* 	Meta intérprete Vanilla	 			*/
/*							*/
/*	2012.						*/
/*	Dpto. de Informática. 				*/
/*	Universidad de Valladolid.			*/
/*							*/
/*	adaptado de:					*/
/*	L. Sterling, E. Shapiro				*/
/*	The Art of Prolog, Second Edition		*/
/*	The MIT Press 1994				*/
/*	ISBN 0-262-19338-8				*/
/*							*/
/*		y:					*/
/*							*/
/*	D. Poole, A. Mackworth				*/
/*	Artificial Intelligence: Foundations		*/
/*	of Computational Agents.			*/
/*	Cambridge Univestity Press, 2010.		*/


/*	Meta intérprete Vanilla				*/
/*							*/

/*	Hay que definir dos nuevos operadores		*/
:-op(40, xfy, &).
:-op(50, xfy, --->).

solve(true):-!.
solve((A & B)) :-!, solve(A), solve(B).
solve(A) :- (B ---> A), solve(B).




/* 	Base conocimiento House Wire	 		*/
/*							*/
/*	2012.						*/
/*	Dpto. de Informática. 				*/
/*	Universidad de Valladolid.			*/
/*							*/
/*	adaptado de:					*/
/*	D. Poole, A. Mackworth				*/
/*	Artificial Intelligence: Foundations		*/
/*	of Computational Agents.			*/
/*	Cambridge Univestity Press, 2010.		*/

/*	disponible en:					*/		
/*	http://artint.info/html/ArtInt.html		*/







light(L)&
ok(L)&
live(L) 
	---> lit(L).


connected_to(W,W1)&
live(W1) 
	---> live(W).

true ---> live(outside).
true ---> light(l1).
true ---> light(l2).
true ---> down(s1).
true ---> up(s2).
true ---> up(s3).
true ---> connected_to(l1,w0).
up(s2) & ok(s2) ---> connected_to(w0,w1).
down(s2) & ok(s2) ---> connected_to(w0,w2).
up(s1) & ok(s1) ---> connected_to(w1,w3).
down(s1) & ok(s1) ---> connected_to(w2,w3).
true ---> connected_to(l2,w4).
up(s3) & ok(s3) ---> connected_to(w4,w3).
true ---> connected_to(p1,w3).
ok(cb1) ---> connected_to(w3,w5).
true ---> connected_to(p2,w6).
ok(cb2) ---> connected_to(w6,w5).
true ---> connected_to(w5,outside).
true ---> ok(_).
