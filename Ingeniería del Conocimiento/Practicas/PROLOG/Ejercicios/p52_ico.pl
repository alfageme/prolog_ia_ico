% Práctica 5 - Ingeniería del Conocimiento 
% Samuel Alfageme Sainz
% -----------------------------------------
% METAINTERPRETES : Ejercicio 2
%
% Modificacion del metainterprete vanilla para que realice una busqueda con 
%			   profundidad limitada : argumento de la llamada.

solve(true,_):-!.
solve((A,B),Prof) :-!, solve(A,Prof), solve(B,Prof).
solve(A,Prof) :- clause(A,B), 
					SubProf is Prof - 1, SubProf >= 0, solve(B, SubProf).

% Representacion de la profundidad: le damos a cada nodo etiquetado wi el valor
% de profundidad i.

valor(w1,1).
conectado(w2, w1).
conectado(w3, w2).
conectado(w4, w3).
conectado(w5, w4).
conectado(w6, w5).
valor(W,X):-conectado(W,V), valor(V,X).

% Consultas de profundidad limitada:
% ? - solve(valor(w5,X),3).
% false.
% ? - solve(valor(w5,X),6).
% X = 1.