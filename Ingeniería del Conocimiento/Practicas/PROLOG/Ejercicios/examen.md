# Examen PROLOG - Ingeniería del Conocimiento

2/12/2014 - Gº Ingenieria Informatica - ETSII UVa

## Ejercicio 1

Modificar el metainterprete para que permita cláusulas del tipo.

```
hecho(X).
regla(A ---> B).
regla(A & B).
```

## Ejercicio 2

Modificar el metainterprete vanilla para que pida confirmación al usuario en 
las cláusulas del tipo `ok(X)`. (*Emplear para ello el predicado predefinido
de swi-prolog `read/1`*)

## Ejercicio 3

Modificar el metainterprete que permite metas diferidas, para que no incluya 
metas repetidas en la lista de metas diferidas.

Yo lo hice aplicando el predicado built-in `memberchk/2` / también es posible
(y quizá mejor) hacerlo con `member/2`
