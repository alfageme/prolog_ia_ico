% Práctica 6 - Ingeniería del Conocimiento 
% Samuel Alfageme Sainz
% -----------------------------------------
% Metainterprete Vanilla con metas diferidas

:-op(40, xfy, &).
:-op(50, xfy, --->).

% +--------------------------------------------------------------+
% |    Ampliación del metainterprete: solve + metas diferidas    |
% +--------------------------------------------------------------+
% dsolve/3: dsolve(M,D1,D2) evalua a true si D1 es la finalizacion
% de D2 y M es consecuencia logica de la base de conocimiento y de 
% las metas diferidas D2

dsolve(true,D,D)	  	:- !.
dsolve((A & B),D1,D3) 	:- !,dsolve(A,D1,D2),dsolve(B,D2,D3).
dsolve(Meta,D,[Meta|D]) :- meta_diferida(Meta).
dsolve(A,D1,D2)       	:- (B ---> A),dsolve(B,D1,D2).

% +--------------------------------------------------------------+
% |                    Base de conocimiento:					 |
% +--------------------------------------------------------------+

% Elementos básicos:
true 			  ---> light(l1).
true 			  ---> light(l2).
true 			  ---> live(outside).
% Estado de los elementos de la red electrica:
true 			  ---> up(s2).
true 			  ---> up(s3).
true 			  ---> down(s1).
% Conexiones directas:
true	          ---> connected_to(w5,outside).
true              ---> connected_to(l1,w0).
true	          ---> connected_to(l2,w4).
true	          ---> connected_to(p1,w3).
true	          ---> connected_to(p2,w6).
% Conexiones condicionadas por un interruptor:
ok(cb1)	          ---> connected_to(w3,w5).
ok(cb2)	          ---> connected_to(w6,w5).
% Coneciones condicionadas por un conmutador:
up(s2)   & ok(s2) ---> connected_to(w0,w1).
up(s1)   & ok(s1) ---> connected_to(w1,w3).
up(s3)	 & ok(s3) ---> connected_to(w4,w3).
down(s2) & ok(s2) ---> connected_to(w0,w2).
down(s1) & ok(s1) ---> connected_to(w2,w3).
% Reglas de funcionamiento de la red eléctrica:
light(L) & ok(L) & live(L) 		---> lit(L).
connected_to(W,W1) & live(W1)   ---> live(W).

meta_diferida(ok(_)).

% Ejemplo de consulta sobre metas diferidas:

% 1 ?- dsolve(lit(l2), [], D).
% D = [ok(cb1), ok(s3), ok(l2)];
% false.

% 2 ?- dsolve(live(p2), [], D).
% D = [ok(cb2)];
% false.

% 3 ?- dsolve(lit(l2), [ok(cb2)], D).
% D = [ok(cb1), ok(s3), ok(l2), ok(cb2)];
% false.