;   Práctica 7 - Ingeniería del Conocimiento 
;            Samuel Alfageme Sainz
; --------------------------------------------
; ASISTENTE AL DIAGNOSTICO - POOLE & MACKWORTH

;	Hechos de Instancia específica:

(deffacts asistente "Asistente al Diagnóstico de Poole/Mackworth"
	(tension outside t)
	(ok cb1 t)
	(ok cb2 t)
	(estado s1 down)
	(estado s2 up)
	(estado s3 up)
	(ok s1 t)
	(ok s2 t)
	(ok s3 t)
	(conectado w5 outside)
	(conectado p1 w3)
	(conectado p2 w6)
	(conectado l1 w0)
	(conectado l2 w4)
	(bombilla l1 t)
	(bombilla l2 t)
	(ok l1 t)
	(ok l2 t)
	(ok p1 t)
	(ok p2 t)
)

(defrule bombillas "Enciende las bombillas"
	(bombilla ?x t)
	(tension ?x t)
	(ok ?x t)
=>
	(assert (luce ?x t))
)

(defrule tension "Transmite la tension de elementos conectados"
	(conectado ?x ?y)
	(tension ?y t)
=>
	(assert (tension ?x t))
)

; 	Reglas de Instancia Específica:

(defrule fusible1 "Funcionamiento del fusible cb1"
	(ok cb1 t)
=>
	(assert (conectado w3 w5))
)

(defrule fusible2 "Funcionamiento del fusible cb2"
	(ok cb2 t)
=>
	(assert (conectado w6 w5))
)

(defrule sUP1 "Funcionamiento UP del switch s1"
	(ok s1 t)
	(estado s1 up)
=>
	(assert (conectado w1 w3))
)

(defrule sDOWN1 "Funcionamiento DOWN del switch s1"
	(ok s1 t)
	(estado s1 down)
=>
	(assert (conectado w2 w3))
)

(defrule sUP2 "Funcionamiento UP del switch s2"
	(ok s2 t)
	(estado s2 up)
=>
	(assert (conectado w0 w1))
)

(defrule sDOWN2 "Funcionamiento DOWN del switch s2"
	(ok s2 t)
	(estado s2 down)
=>
	(assert (conectado w0 w2))
)

(defrule switch_s3 "Funcionamiento normal del switch s2"
	(ok s3 t)
	(estado s3 up)
=>
	(assert (conectado w4 w3))
)
